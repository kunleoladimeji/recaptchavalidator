# Google's Recaptcha Validator using JavaScript

[DEMO](https://goo.gl/f0pAML)

*Structure*
- **css** (for css files) 
- **img** (for image files)
- **inc** (for PHP include files)
- **js** (for JavaScript files)

----------
# Next Updates
- Use composer for dependencies rather than storing them in files
- Some JavaScript tweaks