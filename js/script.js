jQuery(document).ready(function($) {
	$("#contactForm").validate({
		ignore: [],
		rules: {
			fullname: "required",
			email: {
				required: true,
				email: true
			},
			phone: "required",
			message: {
				required: true,
				minlength: 50
			},
			hiddenRecaptcha: {
				required: function () {
					if (grecaptcha.getResponse() == '') {
						return true;
					}
					else {
						return false;
					}
				}
			}
		},
		messages: {
			fullname: "Please specify your full name",
			email: {
				required: "Please specify your email address",
				email: "Your email address must be in the format of name@domain.com"
			},
			phone: "Please specify your phone number",
			message: {
				required: "Please add your message",
				minlength: "Message length cannot be less than 50 characters"
			},
			hiddenRecaptcha: {
				required: "Please tick the captcha"
			}
		},
		submitHandler: function() {
			
			BootstrapDialog.show({
				title: 'Contact Form',
    			message: 'Form validated!',
    			buttons: [{
			        id: 'btn-ok',   
			        icon: 'glyphicon glyphicon-check',       
			        label: 'OK',
			        cssClass: 'btn-primary', 
			        autospin: false,
	        		action: function(dialogRef){ 
	        			$("form").trigger('reset');
	        			grecaptcha.reset();   
	            		dialogRef.close();
	        		}
	    		}]
			});
		}
	});
});